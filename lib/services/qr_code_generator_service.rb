# frozen_string_literal: true

class QrCodeGeneratorService
  def self.call
    new.call
  end

  def call
    generate_image
  end

  private

  def initialize
    @wifi_ssid = ask_ssid
    @wifi_password = ask_password
    @timestamp = Time.now
  end

  def ask_ssid
    input_invite('Wifi SSID')
  end

  def ask_password
    input_invite('Wifi password')
  end

  def input_invite(message)
    printf [message, "\n"].join
    Readline.readline('> ', true)
  end

  def qr_code_svg
    # WIFI:S:<SSID>;T:<WPA|WEP|>;P:<password>;;
    qrcode = RQRCode::QRCode.new(['WIFI:S:', @wifi_ssid, ';T:WPA;P:', @wifi_password, ';;'].join)

    qrcode.as_svg(
      offset: 120,
      module_size: 15,
      color: '222'
    )
  end

  def generate_image
    canevas = Magick::ImageList.new.from_blob(qr_code_svg.to_s)
    canevas.format = 'png'

    annotate(canevas, @wifi_ssid, align: :top_center, color: '#333', font_size: 70, padding_x: 0, padding_y: 20)
    generated_at = @timestamp.strftime('Generated the %d/%m/%Y at %H:%M:%S')
    annotate(canevas, generated_at, align: :top_center, color: '#999', font_size: 15, padding_x: 0, padding_y: 100)
    annotate(canevas, @wifi_password, align: :bottom_center, color: '#333', font_size: 40, padding_x: 0,
                                      padding_y: 30)

    canevas.write(['data/', @timestamp.strftime('%d%m%Y%H%M%S'), '-', @wifi_ssid, '.png'].join)
    canevas.display
  end

  def annotate(canevas, text, **opts)
    img = Magick::Draw.new

    align = POSITIONS[opts[:align]] || Magick::NorthGravity
    color = opts[:color] || '#000'
    font_family = opts[:font_family] || 'Helvetica'
    font_size = opts[:font_size] || 70
    padding_x = opts[:padding_x] || 0
    padding_y = opts[:padding_y] || 30

    canevas.annotate(img, 0, 0, padding_x, padding_y, text) do
      self.gravity = align
      self.pointsize = font_size
      self.fill = color
      self.font_family = font_family
    end
  end
end
