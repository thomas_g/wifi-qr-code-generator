# frozen_string_literal: true

POSITIONS = {
  top_left: Magick::NorthWestGravity,
  top_center: Magick::NorthGravity,
  top_right: Magick::NorthEastGravity,
  middle_left: Magick::WestGravity,
  middle_center: Magick::CenterGravity,
  middle_right: Magick::EastGravity,
  bottom_left: Magick::SouthWestGravity,
  bottom_center: Magick::SouthGravity,
  bottom_right: Magick::SouthEastGravity
}.freeze
