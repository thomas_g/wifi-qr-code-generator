# frozen_string_literal: true

require 'rqrcode'
require 'rmagick'
require 'readline'

require_relative 'constants'
require_relative 'services/qr_code_generator_service'

include Magick
