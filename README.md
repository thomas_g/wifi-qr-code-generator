# Wifi QR code generator

This script will ask for your wifi credentials and generate a QR code in the folder /data.
It relies on the gem [rqrcode](https://github.com/whomwah/rqrcode).

To install it, run `bundle install`.

To run it `ruby .main.rb`.

![Example QR Code](https://gitlab.com/thomas_g/wifi-qr-code-generator/-/raw/master/data/example.png)
